from itertools import *
import numpy as np
import h5py
import pytriqs.utility.mpi as mpi
import textwrap
from pytriqs.archive import *
from pytriqs.gf import *
from triqs_dft_tools.sumk_dft import *
from triqs_dft_tools.sumk_dft_tools import *
from pytriqs.operators.util.hamiltonians import *
from pytriqs.operators.util.U_matrix import *
import scipy
import subprocess
import os
import time

from mpi4py import MPI

# fitting of hybridization function:

def set_deg(nshells, deg_orbs, spin_names):
    deg_shells = []
    deg_orbs = [[2, 4], [0, 1, 3]]
    for ish in range(nshells):
        shell_deg = []
        for iset in deg_orbs:
            dic = dict()
            for iorb in iset:
                for isp in spin_names:
                    dic['{0:}_{1:d}'.format(isp,iorb)] = (np.array([[1.0]]), False)
            shell_deg.append(dic)
        deg_shells.append(shell_deg)
    return deg_shells


def chi(params,iw,orig,edcfg):
    numMats = iw.size/2
    g_fit = Delta_fit_np(params,iw[numMats-edcfg['fit_mats_max']:numMats+edcfg['fit_mats_max']])
    diff = g_fit - orig[numMats-edcfg['fit_mats_max']
                                      :numMats+edcfg['fit_mats_max']]
    chi_val = np.sum(np.abs(diff)**2)
    return chi_val

#def chi_non_deg(params,orig,edcfg):
#    g_fit = Delta_fit(params,orig)
#    diff = g_fit - orig
#    numMats = diff.data.shape[0]/2
#    chi_val = np.sum(np.abs(diff.data[numMats-edcfg['fit_mats_max']
#                                      :numMats+edcfg['fit_mats_max']])**2)
#    return chi_val

def Delta_fit_eps_V_non_deg(eps,v,Delta):
    gSum = 0*Delta
    n_bath = eps.shape[1]
    n_orb = eps.shape[0]
    for ii_bath in range(n_bath):
        gAdd = 0*Delta
        for ii_orb in range(n_orb):
            gAdd[ii_orb,ii_orb] << iOmega_n - eps[ii_orb,ii_bath]
        gAdd << inverse( gAdd )
        for ii_orb in range(n_orb):
            gAdd[ii_orb,ii_orb] << gAdd[ii_orb,ii_orb] * v[ii_orb,ii_bath]
        gSum << gSum + gAdd
    return gSum

def Delta_fit_eps_V(eps,v,Delta):
    gSum = 0*Delta
    gAdd = 0*Delta
    for ii in range(len(eps)):
        gAdd << v[ii]**2 * inverse( iOmega_n - eps[ii] )
        gSum << gSum + gAdd
    return gSum

def Delta_fit_eps_V_np(eps,v,iw):
    eps = np.array(eps)
    v = np.array(v)
    g = np.sum((v**2)[np.newaxis,:] / (1j*iw[:,np.newaxis] - eps[np.newaxis,:]),axis=1)
    return g

def Delta_fit_eps_V_real(eps,v,idelta,Delta):
    g = 0*Delta
    g << Omega + idelta*1j
    g -= eps
    g = inverse(g)
    g *= v**2
    return g

def Delta_fit(params,Delta):
    n_bath = len(params)//2
    eps = params[0:n_bath]
    v = params[n_bath:]
    return Delta_fit_eps_V(eps,v,Delta)

def Delta_fit_np(params,iw):
    n_bath = len(params)//2
    eps = params[0:n_bath]
    v = params[n_bath:]
    return Delta_fit_eps_V_np(eps,v,iw)
    

def fit_hyb_off_diag(Delta_iw,edcfg,guess,n_orb,gf_struct_solver):
    iw = np.array([iw.imag for iw in Delta_iw.mesh ])
    n_bath = edcfg['n bath sites']
    fit_param = np.zeros((2,n_orb,n_bath))
    for key in gf_struct_solver[0]:
        spin, orb = key.split('_')
        startV = edcfg['startV']
        startE = edcfg['startEps']
        if guess is None:
            iters = edcfg['numItersRandFit']
            guess0 = None
        else:
            iters = 1
            guess0 = guess
        for iO in range(n_orb):
            minfm = 1e100
            minPoints = []
            minFuns = []
            mpi.report('fitting orbital'+str(iO))
            mpi.report('delta_0'+str(Delta_iw[key].data[0,iO,iO]))
            for ii in range(iters):
                if guess0 is None:
                    rand = np.random.rand(n_bath,2)
                    x0 = np.hstack([(rand[:,0]+startE[0]/float(startE[1]-startE[0]))
                              *(startE[1]-startE[0]),
                                   (rand[:,1]+startV[0]/float(startV[1]-startV[0]))
                              *(startV[1]-startV[0])])  #initial guess                         
                else:
                    x0 = np.hstack([guess[0][iO,:,0],  guess[1][iO,:,0]])
                ans = scipy.optimize.minimize(chi,x0,
                                              args=(iw,Delta_iw[key].data[:,iO,iO],edcfg),
                                          method='BFGS',options={'disp': False})
                diff = minfm-ans.fun
                minPoints.append(ans.x)
                minFuns.append(ans.fun)
                if diff > 0:
                    # save the new minimum as current minimum                                  
                    minfm = ans.fun
                    # save the new [v,e] as current [v,e]                                      
                    minPoint = ans.x
                    print 'iteration',ii,'diff', diff
                    
                    if diff < edcfg['maxDeltaFitBreak']:
                        break
                #print ans.fun
            #print minFuns
            mini_index = np.argmin(minFuns)
            minPoint = minPoints[mini_index]
            
            print('done:',minPoint)
            #if finished
            fit_param[0,iO,:]=minPoint[:n_bath]
            fit_param[1,iO,:]=minPoint[n_bath:]
        break

    for iO in range(n_orb):
        print "fitting parameters: (eps,V) " ,iO, fit_param[:,iO]
    
    return fit_param

def fit_hyb(Delta_iw,edcfg,repr_orbs,guess):
    iw = np.array([iw.imag for iw in Delta_iw.mesh ])
    n_orb = len(repr_orbs)
    n_bath = edcfg['n bath sites']
    fit_param = np.zeros((2,n_orb,n_bath))
    for iOrb, key in enumerate(repr_orbs):
        mpi.report('fitting orbital '+key)
        minfm = 1e100
        startV = edcfg['startV']
        startE = edcfg['startEps']
        if guess is None:
            iters = edcfg['numItersRandFit']
        else:
            iters = 1
        for ii in range(iters):
            if guess is None:
                rand = np.random.rand(n_bath,2)
                x0 = np.hstack([(rand[:,0]+startE[0]/float(startE[1]-startE[0]))
                          *(startE[1]-startE[0]),
                               (rand[:,1]+startV[0]/float(startV[1]-startV[0]))
                          *(startV[1]-startV[0])])  #initial guess                         
            else:
                spin, orb = key.split('_')
                orb = int(orb)
                x0 = np.hstack([guess[0][orb,:,0],  guess[1][orb,:,0]])
            ans = scipy.optimize.minimize(chi,x0,
                                          args=(iw,Delta_iw[key].data[:,0,0],edcfg),
                                      method='BFGS',options={'disp': False})
            diff = minfm-ans.fun

            if diff > 0:
                # save the new minimum as current minimum                                  
                minfm = ans.fun
                # save the new [v,e] as current [v,e]                                      
                minPoint = ans.x
                print 'iteration',ii,'diff', diff
                if diff < edcfg['maxDeltaFitBreak']:
                    break
        print(minPoint)
        fit_param[0,iOrb,:]=minPoint[:n_bath]
        fit_param[1,iOrb,:]=minPoint[n_bath:]

    for iOrb, key in enumerate(repr_orbs):
        print "fitting parameters: (eps,V) " ,key, fit_param[:,iOrb]
    
    return fit_param

def fit_hyb_non_deg(Delta_iw,edcfg,gf_struct,guess):
    raise Exception('todo')
    fit_param = []
    for iOrb, key in enumerate(gf_struct):
        fit_param_ = []
        if 'down' in key:
            continue
        for iO in gf_struct[key]:
            mpi.report('fitting orbital '+key+' '+str(iO))
            minfm = 1e100
            startV = edcfg['startV']
            startE = edcfg['startEps']
            if guess is None:
                iters = edcfg['numItersRandFit']
            else:
                iters = 1
                spin, orb = key.split('_')
                orb = int(orb)
                x0 = np.array([guess[0][0,iO,0],  guess[1][0,iO,0]])
            for ii in range(iters):
                if guess is None:
                    x0 = np.array([(np.random.rand()+startE[0]/float(startE[1]-startE[0]))
                          *(startE[1]-startE[0]),
                          (np.random.rand()+startV[0]/float(startV[1]-startV[0]))
                          *(startV[1]-startV[0])])  #initial guess
                    
                ans = scipy.optimize.minimize(chi_non_deg,x0,
                                                  args=(Delta_iw[key][iO,iO],edcfg),
                                      method='BFGS',options={'disp': False})
                diff = minfm-ans.fun
                
                if diff > 0:
                    # save the new minimum as current minimum
                    minfm = ans.fun
                    # save the new [v,e] as current [v,e]
                    minPoint = ans.x
                    print 'iteration',ii,'diff', diff
                    if diff < edcfg['maxDeltaFitBreak']:
                        break
            fit_param_.append(minPoint)
        fit_param.append(fit_param_)

    for iOrb, key in enumerate(gf_struct):
        if 'down' in key:
            continue
        for iO in gf_struct[key]:
            print "fitting parameters: (eps,V) " ,key,iO, fit_param[iOrb][iO]
    
    return fit_param



def distr_fit_to_deg_orbs(fit_param,struct_solver,deg_shells,n_orb,n_bath):

    # distribute the results from deg shells to all shells 
    n_bath
    num_block_deg_orbs = len(deg_shells)

    fit_param_all = np.zeros((2,n_orb,n_bath))
    for iOrb,key in enumerate(struct_solver):
        spin, orb = key.split('_')
        orb = int(orb)
        for iblock in range(num_block_deg_orbs):
            if key in deg_shells[iblock].keys():
                fit_param_all[:,orb,:] = fit_param[:,iblock,:]
    epsk = np.zeros((n_orb,n_bath,2),dtype=float)
    epsk[:,:,0] = fit_param_all[0,:,:]
    epsk[:,:,1] = fit_param_all[0,:,:]

    Vk = np.zeros((n_orb,n_bath,2),dtype=float)
    Vk[:,:,0] = fit_param_all[1,:,:]
    Vk[:,:,1] = fit_param_all[1,:,:]

    return epsk, Vk

def distr_fit(fit_param):

    n_orb = fit_param.shape[1]
    n_bath = fit_param.shape[2]
    # distribute the results from deg shells to all shells 
    #n_bath
    #num_block_deg_orbs = len(deg_shells)

    #fit_param_all = np.zeros((2,n_orb,n_bath))
    #for iOrb,key in enumerate(struct_solver):
    #    spin, orb = key.split('_')
    #    orb = int(orb)
    #    for iblock in range(num_block_deg_orbs):
    #        if key in deg_shells[iblock].keys():
    #            fit_param_all[:,orb,:] = fit_param[:,iblock,:]
    epsk = np.zeros((n_orb,n_bath,2),dtype=float)
    epsk[:,:,0] = fit_param[0,:,:]
    epsk[:,:,1] = fit_param[0,:,:]

    Vk = np.zeros((n_orb,n_bath,2),dtype=float)
    Vk[:,:,0] = fit_param[1,:,:]
    Vk[:,:,1] = fit_param[1,:,:]

    return epsk, Vk

def remove_old_run():
    try:
        os.remove('end_file.dat')
    except:
        pass

def wait_solver(myrank):
    # randomized waiting time so that not all ranks check for
    # existing file at the same time
    exists = False
    while exists == False:
        exists = os.path.isfile('end_file.dat')
        adder = 2+((np.random.rand()*(myrank+1)*100)%100)/50
        time.sleep(1+adder )
def run_solver(edcfg):
    
    print 'running ED_lib'
    time.sleep(10)
    MPI.COMM_SELF.Spawn(edcfg['eigen executable'], args=[edcfg["eigen input"], ],
                        maxprocs=edcfg["eigen ncores"])
    
    return

def read_static(obsv):
    with h5py.File('sim.h5','r') as ar:
        data = ar['results/static_observables/'+obsv][...]
    return data


def read_real_data_off_diag(edcfg):

    with h5py.File('sim.h5','r') as ar:
        data_ = ar['results/G_ij_omega_r/data'][...]
        w = ar['results/G_ij_omega_r/mesh/1/points'][...]
    
    data = np.zeros((w.size,edcfg['n_orbs'],edcfg['n_orbs'],2),
                     dtype=complex)
    
    for iO in range(edcfg['n_orbs']):
        for jO in range(iO,edcfg['n_orbs']):
            data[:,iO,jO,:] = data_[:,iO*edcfg['n_orbs']+jO,:,0] + 1j*data_[:,iO*edcfg['n_orbs']+jO,:,1]
            if iO != jO:
                data[:,jO,iO,:] = data[:,iO,jO,:]
    return w, data

def read_real_data():

    with h5py.File('sim.h5','r') as ar:
        data = ar['results/G_omega_r/data'][...]
        w = ar['results/G_omega_r/mesh/1/points'][...]

    data = data[...,0] + 1j*data[...,1]

    return w, data

def read_matsub_data(edcfg):

    with h5py.File('sim.h5','r') as ar:
        data_ = ar['results/G_omega/data'][...]

    data = np.zeros((edcfg['n matsub']*2,edcfg['n_orbs'],2),
                     dtype=complex)
    for iO in range(edcfg['n_orbs']):
        data[edcfg['n matsub']:,iO,:] = data_[:,iO,:,0] + 1j*data_[:,iO,:,1]
        data[:edcfg['n matsub'],iO] = data_[::-1,iO,:,0] - 1j*data_[::-1,iO,:,1]
    print(data.shape)
    return data

def read_matsub_data_off_diag(edcfg):

    with h5py.File('sim.h5','r') as ar:
        data_ = ar['results/G_ij_omega/data'][...]

    data = np.zeros((edcfg['n matsub']*2,edcfg['n_orbs'],edcfg['n_orbs'],2),
                     dtype=complex)
    for iO in range(edcfg['n_orbs']):
        for jO in range(iO,edcfg['n_orbs']):
            data[edcfg['n matsub']:,iO,jO,:] = data_[:,iO*edcfg['n_orbs']+jO,:,0] + 1j*data_[:,iO*edcfg['n_orbs']+jO,:,1]
            data[:edcfg['n matsub'],iO,jO,:] = data_[::-1,iO*edcfg['n_orbs']+jO,:,0] - 1j*data_[::-1,iO*edcfg['n_orbs']+jO,:,1]
            if iO != jO:
                data[:,jO,iO,:] = data[:,iO,jO,:]
    return data

def set_solver(Umat,eal,Vk,epsk,edcfg):

    n_orbs = eal['up'].shape[0]

    sectors = set_sectors(edcfg)

    t0 = np.zeros((n_orbs,n_orbs,2),dtype=float)
    t0[:,:,0] = eal['up']
    t0[:,:,1] = eal['down']
    for iO in range(n_orbs):
        t0[iO,iO,:] = 0.0

    data = h5py.File("input.h5", "w");

    gf = data.create_group("GreensFunction_orbitals")
    gf.create_dataset("values",data = edcfg["GreensFunction_orbitals"])
    
    chi = data.create_group("ChiLoc_orbitals")
    chi.create_dataset("values",data = edcfg["ChiLoc_orbitals"])
    
    hop_g = data.create_group("sectors")
    hop_g.create_dataset("values", data=sectors)
    bath = data.create_group("Bath")

    EPSK = []
    VK = []
    for iO in range(n_orbs):
        EPSK.append(epsk[iO,:,:])
        VK.append(Vk[iO,:,:])

    for iO in range(n_orbs):
        Epsk_g = bath.create_group("Epsk_" + str(iO))
        Epsk_g.create_dataset("values", data=EPSK[iO])
        Vk_g = bath.create_group("Vk_" + str(iO))
        Vk_g.create_dataset("values",data=VK[iO])
        t0_g = data.create_group("t0_" + str(iO))
        t0_g.create_dataset("values", data=np.array(t0[iO,:,:]))


    
    Eps0 = np.zeros((n_orbs,2),dtype=float)
    Eps0[:,0] = np.diag(eal['up'])
    Eps0[:,1] = np.diag(eal['down'])

    hop_g = data.create_group("Eps0")
    hop_g.create_dataset("values", data=Eps0)


    int_ds = data.create_dataset("mu", shape=(), data=0.0)

    int_g = data.create_group("interaction")
    int_ds = int_g.create_dataset("values", shape=(n_orbs,n_orbs,n_orbs,n_orbs,), data=Umat.real)

    #edcfg = _set_ed_defaults(edcfg)

    write_ed_input('anderson.param',**edcfg)


def set_solver_all(Umat,eal,Vk,epsk,edcfg):

    n_orbs = eal['up'].shape[0]

    sectors = set_sectors_all(edcfg)

    t0 = np.zeros((n_orbs,n_orbs,2),dtype=float)
    t0[:,:,0] = eal['up']
    t0[:,:,1] = eal['down']
    for iO in range(n_orbs):
        t0[iO,iO,:] = 0.0

    data = h5py.File("input.h5", "w");

    hop_g = data.create_group("sectors")
    hop_g.create_dataset("values", data=sectors)
    bath = data.create_group("Bath")

    for iO in range(n_orbs):
        Epsk_g = bath.create_group("Epsk_" + str(iO))
        Epsk_g.create_dataset("values", data=epsk[:,iO,:])
        Vk_g = bath.create_group("Vk_" + str(iO))
        Vk_g.create_dataset("values", data=np.array(Vk[:,iO,:]))
        t0_g = data.create_group("t0_" + str(iO))
        t0_g.create_dataset("values", data=np.array(t0[iO,:,:]))


    
    Eps0 = np.zeros((n_orbs,2),dtype=float)
    Eps0[:,0] = np.diag(eal['up'])
    Eps0[:,1] = np.diag(eal['down'])

    hop_g = data.create_group("Eps0")
    hop_g.create_dataset("values", data=Eps0)


    int_ds = data.create_dataset("mu", shape=(), data=0.0)

    int_g = data.create_group("interaction")
    int_ds = int_g.create_dataset("values", shape=(n_orbs,n_orbs,n_orbs,n_orbs,), data=Umat.real)

    #edcfg = _set_ed_defaults(edcfg)

    write_ed_input('anderson.param',**edcfg)

def set_sectors_all(edcfg):
    if edcfg['all_sectors']:
        sectors = []
        partNum_min = edcfg['fock min']
        partNum_max = edcfg['fock max']
        for ii in range(partNum_min,partNum_max+1):
            if ii%2 == 0:
                sectors.append([ii//2,ii//2])
            else:
                sectors.append([ii//2+1,ii//2])
                sectors.append([ii//2,ii//2+1])
            lastSector = sectors[-1]
            for ipm in range(1,edcfg['spin pm']+1):
                res1 = lastSector[0] - ipm
                res2 = lastSector[1] + ipm
                if res1 < 0 or res2 > 2*edcfg["n_orbs"]*(1+edcfg["n bath sites"]):
                    pass
                else:
                    sectors.append([res1,res2])
                    sectors.append([res2,res1])
        sectors = np.array(sectors)
    else:
        sectors = np.array(edcfg['sectors'])

    return sectors


def set_sectors(edcfg):
    if edcfg['all_sectors']:
        sectors = []
        partNum_min = edcfg['fock min']
        partNum_max = edcfg['fock max']
        for ii in range(partNum_min,partNum_max+1):
            if ii%2 == 0:
                sectors.append([ii//2,ii//2])
            else:
                sectors.append([ii//2+1,ii//2])
                sectors.append([ii//2,ii//2+1])
            lastSector = sectors[-1]
            for ipm in range(1,edcfg['spin pm']+1):
                res1 = lastSector[0] - ipm
                res2 = lastSector[1] + ipm
                if res1 < 0 or res2 > 2*edcfg["n_orbs"]*(1+edcfg["n bath sites"]) or (
                        res1 == 6 and res2 == 11 
                ) or (
                    res1 == 7 and res2 == 11
                    ):
                    pass
                else:
                    sectors.append([res1,res2])
                    sectors.append([res2,res1])
        sectors = np.array(sectors)
    else:
        sectors = np.array(edcfg['sectors'])

    return sectors

def _set_ed_defaults(edcfg):
    """Set the defaults for the ed solver."""

    max_n = _max_n_imp(edcfg["n_orbs"])
    max_np = _max_n_impbath(edcfg["n_orbs"], edcfg["n bath sites"])

    edcfg.setdefault("n_imp_min", 0)
    edcfg.setdefault("n_imp_max", max_n)
    edcfg.setdefault("fock min", 0)
    edcfg.setdefault("fock max", max_np)

    #edcfg.setdefault("n omega", edcfg["n energies"])
    #edcfg.setdefault("n matsub", edcfg["n energies"])

    edcfg.setdefault("n omega extra", 0)
    edcfg.setdefault("n matsub extra", 0)

    edcfg.setdefault("n omega extra factor", 100)
    edcfg.setdefault("n matsub extra factor", 100)

    edcfg.setdefault("n lancz iter", 50)
    edcfg.setdefault("n lancz states", 50)

    return edcfg

def write_ed_input(path, **edcfg):
    """Write the config file for the ED solver."""
    ham = textwrap.dedent(_INPUT_SKELETON.format(**edcfg))

    with open(path, "w") as ham_file:
        ham_file.write(ham)

    return ham

def _max_n_imp(n_orbs):
    """Max number of orbitals."""
    return 2 * n_orbs


def _max_n_impbath(n_orbs, n_bathsites):
    """Max number of cluster sites."""
    return 2 * n_orbs + 2 * n_bathsites * n_orbs
def write_ed_hamilton(path, **edcfg):
    """Write the config file for the ED solver."""
    ham = textwrap.dedent(_HAMILTON_SKELETON.format(**edcfg))

    with open(path, "w") as ham_file:
        ham_file.write(ham)

    return ham
    

def write_two_particle(Umat):
    n_orbs = Umat.shape[0]
    with open('coulomb_matrix.dat', 'w') as f:
        f.write("0\n")
        for i in range(n_orbs):
            for j in range(n_orbs):
                for k in range(n_orbs):
                    for l in range(n_orbs):
                        f.write("%d %d %d %d %d %d %19.15f"%(0,0,i+1,j+1,k+1,l+1,Umat[i,k,j,l].real))
                        f.write("\n")


def write_crys_field(eal):
    n_orbs = eal['up'].shape[0]
    diag_h=np.zeros((2*n_orbs,2*n_orbs))
    diag_h[:n_orbs,:n_orbs] = eal['up']
    diag_h[n_orbs:,n_orbs:] = eal['down']
    np.savetxt('crystalfield.dat', diag_h)

def write_crys_field_from_S(hloc,n_orbs):
    diag_h=np.zeros((2*n_orbs,2*n_orbs))

    for ii in hloc:
        #print ii
        #print ii[0] 
        #print len(ii[0])
        op1 = ii[0][0][1][0]
        op2 = ii[0][1][1][0]
        val = ii[1]
        #print op1
        #print op2
        #print val
        if len(ii[0]) == 2:
            if  'up' in op1:
                #data= np.array([ ii[0][0][1][1], ii[0][1][1][1], ii[1]])
                index1=op1.split('_')[1]
                index2=op2.split('_')[1]
                diag_h[int(index1),int(index2)]= val.real
                
            if 'down' in op1:
                #data= np.array([ii[0][0][1][1], ii[0][1][1][1], ii[1]])
                index1=op1.split('_')[1]
                index2=op2.split('_')[1]
                diag_h[int(index1)+n_orbs,int(index2)+n_orbs]= val.real


    np.savetxt('crystalfield.dat', diag_h)



_INPUT_SKELETON = """\
NSITES={n_total}
NSPINS=2
INPUT_FILE=input.h5

[storage]
MAX_DIM=4900
MAX_SIZE=800000
EIGENVALUES_ONLY=0

[arpack]
NEV={NEV}
NCV={NCV}
SECTOR=FALSE

[lanc]
NLANC={NLANC}
BOLTZMANN_CUTOFF={boltzmann_cutoff}
NOMEGA={n matsub}
BETA={beta}
EMIN={omega min}
EMAX={omega max}
[siam]
NORBITALS={n_orbs}
"""


_SITE_SKELETON = """\
site {{
  eb     {eps}
  tb     {hop}
  state  {state}
}}
"""


def metalbath(parameters, path=None):
    """Write the metalbath file for the ED solver.                              
                                                                                
    Arguments                                                                   
    ---------                                                                   
    parameters : list like                                                      
        An array or list with the parameters from the bath fitting.             
        It contains eps and hop.                                                
    path: string  (default=None)                                                
        Path to the location of the file.                                       
                                                                                
    Returns                                                                     
    -------                                                                     
    Returns the bath as string after writing it to the file.                    
                                                                                
    """
    bath = _build_bath(parameters)
    _save_bath(bath, path)

    return bath

def _save_bath(bath, path):
    if path is not None:
        with open(path, "w") as metalbath_file:
            metalbath_file.write(bath)


def _build_bath(parameters):

    dim = len(parameters) * len(parameters[0]) // 2
    bath = "bath_generic {\n"

    idx = 0
    for _ in range(2):
        for orb_parameters in parameters:
            idx, bath = _append_bath_site(bath, orb_parameters, idx, dim)

    bath += "}"

    return bath

def _append_bath_site(bath, parameters, idx, dim):

    eps_list, hop_list = extract_parameters(parameters)
    for eps, hop in zip(eps_list, hop_list):
        state = _metalbath_state(idx, dim)
        idx += 1
        bath += _SITE_SKELETON.format(eps=eps, hop=hop, state=state)

    return idx, bath

def extract_parameters(parameters):
    """Seperate parameters."""
    dim = len(parameters)
    eps_list = parameters[:dim // 2]
    hop_list = parameters[dim // 2:]

    return eps_list, hop_list

def _metalbath_state(idx, dim):
    return "  ".join("1.0" if jdx == idx else "0.0" for jdx in range(2 * dim))

