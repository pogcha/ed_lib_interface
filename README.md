# Examples
## NiO
In the folder `NiO` first run `python converter.py` to convert the dft data to triqs inputand then run the dmft loop with `mpirun -np 20 -oversubscribe python NiO.py`.

## NiO csc
Charge self-consistent NiO calculation. First copy CHG, CHGCAR and WAVECAR from self-consistent VASP calculation into the folder. Then run the dmft loop with `triqs_vasp_csc.sh -n 20 -i 20 -v vasp_intel.sh -d NiO.py` or `qsub que.sh`.