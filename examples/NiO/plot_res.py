#from pytriqs.gf import gf_fnt, GfImTime, InverseFourier, GfLegendre, MatsubaraToLegendre, LegendreToMatsubara, inverse
from pytriqs.archive import HDFArchive
import matplotlib.pyplot as plt
import numpy as np
from triqs_dft_tools.sumk_dft import *
from pytriqs.gf import *
from pytriqs.archive import HDFArchive
from pytriqs.operators.util import *
from triqs_cthyb import *
import pytriqs.utility.mpi as mpi
import ED_lib_interface as E
import h5py



w_ED, G_imp_w = E.read_real_data()

ar = HDFArchive('H_fBZ.proj.h5', 'r')

out = ar['DMFT_results']
iteration_offset = out['iteration_count']+1

Delta_1 = out['Iterations']['Delta_it'+str(iteration_offset-1)]['up_0'].data
Delta_1_F = out['Iterations']['Delta_F_it'+str(iteration_offset-1)]['up_0'].data

Delta_2 = out['Iterations']['Delta_it'+str(iteration_offset-1)]['up_2'].data
Delta_2_F = out['Iterations']['Delta_F_it'+str(iteration_offset-1)]['up_2'].data




G0 = out['Iterations']['G0_iw_loc_it'+str(iteration_offset-1)]
G0_1 = out['Iterations']['G0_iw_loc_it'+str(iteration_offset-1)]['up_0'].data
G0_1_F = out['Iterations']['G0_iw_loc_F_it'+str(iteration_offset-1)]['up_0'].data

G0_2 = out['Iterations']['G0_iw_loc_it'+str(iteration_offset-1)]['up_2'].data
G0_2_F = out['Iterations']['G0_iw_loc_F_it'+str(iteration_offset-1)]['up_2'].data

Sigma_1 = out['Iterations']['Sigma_it'+str(iteration_offset-1)]['up_0'].data
Sigma_2 = out['Iterations']['Sigma_it'+str(iteration_offset-1)]['up_2'].data

G_w = out['Iterations']['G_w_it'+str(iteration_offset-1)]['up'].data

Gimp_1 = out['Iterations']['Gimp_it'+str(iteration_offset-1)]['up_0']
Gimp_2 = out['Iterations']['Gimp_it'+str(iteration_offset-1)]['up_2']
tmp = np.genfromtxt('G_omega')

iw_ed = tmp[:,0]
iw_ed = np.zeros(2*tmp.shape[0])
iw_ed[:tmp.shape[0]] = -tmp[::-1,0]
iw_ed[tmp.shape[0]:] = tmp[:,0]
Gimp_ed_1 = np.zeros((2*tmp.shape[0],1,1),dtype=complex)
Gimp_ed_1[:tmp.shape[0],0,0] = tmp[::-1,1] - 1j*tmp[::-1,2]
Gimp_ed_1[tmp.shape[0]:,0,0] = tmp[:,1] + 1j*tmp[:,2]
Gimp_ed_2 = np.zeros((2*tmp.shape[0],1,1),dtype=complex)
Gimp_ed_2[:tmp.shape[0],0,0] = tmp[::-1,9] - 1j*tmp[::-1,10]
Gimp_ed_2[tmp.shape[0]:,0,0] = tmp[:,9] + 1j*tmp[:,10]



tmp = np.genfromtxt('indra/GF_matsub_1_1.dat')
iw_indra = tmp[:,0]
iw_indra = np.zeros(2*tmp.shape[0])
iw_indra[:tmp.shape[0]] = -tmp[::-1,0]
iw_indra[tmp.shape[0]:] = tmp[:,0]
Gimp_indra_1 = np.zeros((2*tmp.shape[0],1,1),dtype=complex)
Gimp_indra_1[:tmp.shape[0],0,0] = tmp[::-1,1] - 1j*tmp[::-1,2]
Gimp_indra_1[tmp.shape[0]:,0,0] = tmp[:,1] + 1j*tmp[:,2]
tmp = np.genfromtxt('indra/GF_matsub_3_3.dat')
Gimp_indra_2 = np.zeros((2*tmp.shape[0],1,1),dtype=complex)
Gimp_indra_2[:tmp.shape[0],0,0] = tmp[::-1,1] - 1j*tmp[::-1,2]
Gimp_indra_2[tmp.shape[0]:,0,0] = tmp[:,1] + 1j*tmp[:,2]

G0_w = out['Iterations']['G0_w_it'+str(iteration_offset-1)]['up'].data
Sigma_w_1 = out['Iterations']['Sigma_w_it'+str(iteration_offset-1)]['up_0'].data
Sigma_w_2 = out['Iterations']['Sigma_w_it'+str(iteration_offset-1)]['up_2'].data
G_dummy = out['Iterations']['G_w_it'+str(iteration_offset-1)]
Delta_dummy = out['Iterations']['Delta_F_it'+str(iteration_offset-1)]['up_0']
n_iw = Delta_1[0].shape[0]
#print [ii for ii in Delta_dummy.mesh]
iw = [ii.value.imag for ii in Delta_dummy.mesh]
w = [ii.value.real for ii in G_dummy.mesh]
eal =  out['Iterations']['eal_it'+str(iteration_offset-1)]['up']



with h5py.File('sim.h5','r') as ar:
    edlibocc = ar['results/static_observables/N'][...]
    print(edlibocc)
edlibocc = [edlibocc[0],edlibocc[2]]
print('EDlib',edlibocc[0]/2,edlibocc[1]/2,3*edlibocc[0]+2*edlibocc[1])
from python_lib import greens_functions as green

print(Gimp_ed_1.shape)
print(Gimp_1.data.shape)


den1 = green.matsubara_sum_cum(Gimp_ed_1,50.0,iw_ed,10,1,1.0)[0]
den2 = green.matsubara_sum_cum(Gimp_ed_2,50.0,iw_ed,10,1,1.0)[0]
print('matsub_cum/EDlib',den1.real, den2.real, 6*den1.real + 4*den2.real)
#print('matsub_triqs/EDlib',Gimp_1.density()[0,0].real, Gimp_2.density()[0,0].real,6*Gimp_1.density()[0,0].real+4*Gimp_2.density()[0,0].real)
den_indra_1 = green.matsubara_sum_cum(Gimp_indra_1,50.0,iw_indra,10,1,1.0)[0]
den_indra_2 = green.matsubara_sum_cum(Gimp_indra_2,50.0,iw_indra,10,1,1.0)[0]

indraocc = [ 0.532696,  0.999537        ]
print('indra',indraocc[0],indraocc[1],6*indraocc[0]+4*indraocc[1])
print('matsub_cum/indra',den_indra_1.real,den_indra_2.real,6*den_indra_1.real + 4*den_indra_2.real)



dd = 0*G0
dd << iOmega_n
dd -= inverse(G0)

plt.figure(1)
plt.suptitle('Delta')
plt.subplot(2,1,1)
plt.plot(iw,Delta_1[:,0,0].real,'-g')
plt.plot(iw,Delta_1_F[:,0,0].real,'--g',label='fit t2g')

plt.plot(iw,Delta_2[:,0,0].real,'-b')
plt.plot(iw,Delta_2_F[:,0,0].real,'--b',label='fit eg')

plt.subplot(2,1,2)
plt.plot(iw,Delta_1[:,0,0].imag,'-g')
plt.plot(iw,Delta_1_F[:,0,0].imag,'--g',label='fit t2g')

plt.plot(iw,Delta_2[:,0,0].imag,'-b')
plt.plot(iw,Delta_2_F[:,0,0].imag,'--b',label='fit eg')

plt.legend()


plt.figure(5)
plt.suptitle('local DOS')
plt.subplot(3,1,1)
plt.plot(w,-G_w[:,0,0].imag,'g',label='t2g')
plt.plot(w,-G_w[:,2,2].imag,'b',label='eg')
plt.plot(w,-G_w[:,5,5].imag,'r',label='O')
plt.legend()
plt.subplot(3,1,2)
plt.plot(w,-G0_w[:,0,0].imag,'g')
plt.plot(w,-G0_w[:,2,2].imag,'b')
plt.plot(w,-G0_w[:,5,5].imag,'r')

plt.subplot(3,1,3)
plt.plot(w_ED, -G_imp_w[:,0,0].imag,'g')
plt.plot(w_ED, -G_imp_w[:,2,0].imag,'b')



plt.figure(6)
plt.suptitle('Sigma')
plt.subplot(2,2,1)
plt.plot(w,Sigma_w_1[:,0,0].real,'g')
plt.plot(w,Sigma_w_2[:,0,0].real,'b')

plt.subplot(2,2,3)
plt.plot(w,Sigma_w_1[:,0,0].imag,'g')
plt.plot(w,Sigma_w_2[:,0,0].imag,'b')


plt.subplot(2,2,2)
plt.plot(iw,Sigma_1[:,0,0].real,'-g')

plt.plot(iw,Sigma_2[:,0,0].real,'-b')

plt.subplot(2,2,4)
plt.plot(iw,Sigma_1[:,0,0].imag,'-g')

plt.plot(iw,Sigma_2[:,0,0].imag,'-b')

plt.legend()
plt.show()

del ar
