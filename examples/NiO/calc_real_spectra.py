from itertools import *
import numpy as np
import pytriqs.utility.mpi as mpi
from pytriqs.archive import *
from pytriqs.gf import *
from triqs_dft_tools.sumk_dft import *
from triqs_dft_tools.sumk_dft_tools import *
from pytriqs.operators.util.hamiltonians import *
from pytriqs.operators.util.U_matrix import *
from triqs_cthyb import *
import ED_lib_interface as E
from pytriqs.gf.descriptors import Function
from pytriqs.plot.mpl_interface import oplot


filename = 'H_fBZ.proj'

SK = SumkDFT(hdf_file = filename+'.h5', use_dft_blocks = False)


beta = 50.0 # should be 10... for now this is quicker!                                                                                                                  



Sigma = SK.block_structure.create_gf(beta=beta)
spin_dic = {'up':0,'down':1}
SK.put_Sigma([Sigma])
G = SK.extract_G_loc()
SK.analyse_block_structure_from_gf(G, threshold = 1e-3)
for i_sh in range(len(SK.deg_shells)):
    num_block_deg_orbs = len(SK.deg_shells[i_sh])
    mpi.report('found {0:d} blocks of degenerate orbitals in shell {1:d}'.format(num_block_deg_orbs, i_sh))
    for iblock in range(num_block_deg_orbs):
        mpi.report('block {0:d} consists of orbitals:'.format(iblock))
        for keys in SK.deg_shells[i_sh][iblock].keys():
            mpi.report('  '+keys)
repr_orbs = []

num_block_deg_orbs = len(SK.deg_shells[0])
for iblock in range(num_block_deg_orbs):
    repr_orbs.append(SK.deg_shells[0][iblock].keys()[0])

# Setup CTQMC Solver                                                                                                                                                    
n_orb = SK.corr_shells[0]['dim']
spin_names = ['up','down']
orb_names = [i for i in range(0,n_orb)]
orb_hyb = False

#gf_struct = set_operator_structure(spin_names, orb_names, orb_hyb)                                                                                                     
gf_struct = SK.gf_struct_solver[0]
mpi.report('Sumk to Solver: %s'%SK.sumk_to_solver)
mpi.report('GF struct sumk: %s'%SK.gf_struct_sumk)
mpi.report('GF struct solver: %s'%SK.gf_struct_solver)


if mpi.is_master_node():
    ar = HDFArchive(filename+'.h5','a')
    if 'iteration_count' in ar['DMFT_results']: 
        previous_present = True
        iteration_offset = ar['DMFT_results']['iteration_count']+1
        #SK
        SK.dc_imp = ar['DMFT_results']['Iterations']['dc_imp'+str(iteration_offset-1)]
        #epsk = ar['DMFT_results']['Iterations']['epsk_it'+str(iteration_offset-1)]
        #Vk = ar['DMFT_results']['Iterations']['Vk_it'+str(iteration_offset-1)]
        eal = ar['DMFT_results']['Iterations']['eal_it'+str(iteration_offset-1)]
        dc_energ = ar['DMFT_results']['Iterations']['dc_energ'+str(iteration_offset-1)]
        SK.chemical_potential = ar['DMFT_results']['Iterations']['chemical_potential'+str(iteration_offset-1)].real

mpi.barrier()
SK.chemical_potential = mpi.bcast(SK.chemical_potential)
SK.dc_imp = mpi.bcast(SK.dc_imp)
spin_dic = {'up':0,'down':1}






n_w = 0
w = 0
if mpi.is_master_node(): 
    w, G_imp_w = E.read_real_data()

    n_w = w.size
n_w = mpi.bcast(n_w)
w  =mpi.bcast(w)

real_GF = BlockGf(name_block_generator=[(block, GfReFreq(indices=inner, n_points = n_w, window = [w[0], w[-1]])) for block, inner in SK.gf_struct_solver[0].iteritems()],make_copies=False)

sigma = [0*real_GF]

if mpi.is_master_node(): 
    G_ed = 0*real_GF

    for iOrb,key in enumerate(SK.gf_struct_solver[0]):
        spin, orb = key.split('_')
        orb = int(orb)
        print(orb,spin_dic[spin])
        
        G_ed[key].data[:,0,0] = G_imp_w[:,orb,spin_dic[spin]]
    inverse_G0_iw_F = 0*G_ed
    idelta = 0.05
    
    for key in SK.gf_struct_solver[0]:
        spin, orb = key.split('_')
        orb = int(orb)
        epsd_ = eal[spin][orb,orb]
        #v_ = Vk[0,orb,spin_dic[spin]]
        idelta = 0.05
        #epsd_ = 2.0
        #epsk_ = epsk[0,orb,spin_dic[spin]]
        inverse_G0_iw_F[key] << Omega + idelta*1j
        inverse_G0_iw_F[key] -= epsd_
        
    sigma = [inverse_G0_iw_F - inverse(G_ed)]
sigma = mpi.bcast(sigma)

SK.put_Sigma(Sigma_imp=sigma)



import matplotlib.pyplot as plt
if mpi.is_master_node() and False:
    plt.figure(1)
    plt.subplot(2,1,1)
    oplot(G_ed['up_0'],'-',mode='R',name='test')
    oplot(inverse(inverse_G0_iw_F)['up_0'],'-',mode='R',name='fit')
    #plt.plot(w,inverse(inverse_G0_iw_F)['up_0'].data[:,0,0].real,label='fit')
    #plt.plot(w,G_ed['up_0'].data[:,0,0].real,label='orig')

    plt.subplot(2,1,2)
    oplot(G_ed['up_0'],'-',mode='S',name='test')
    oplot(inverse(inverse_G0_iw_F)['up_0'],'-',mode='S',name='fit')
    #plt.plot(w,-inverse(inverse_G0_iw_F)['up_0'].data[:,0,0].imag,label='fit')
    #plt.plot(w,-G_ed['up_0'].data[:,0,0].imag,label='orig')

    plt.figure(2)
    plt.subplot(2,1,1)
    oplot(sigma[0]['up_0'],mode='R')

    plt.subplot(2,1,2)
    oplot(sigma[0]['up_0'],mode='I')


    plt.show()




mu = SK.chemical_potential

#mu = SK.calc_mu(precision=0.0001)
#mpi.barrier()
#mpi.report('mu',mu.real)
#mpi.report('mu1',mu1.real)

ikarray = numpy.array(range(SK.n_k))


mesh_parameters = (w[0],w[-1],len(w))

# generate one dummy Greendunction
G_latt = SK.lattice_gf(
                    ik=0, mu=mu, iw_or_w='w', with_Sigma=False, 
                    with_dc=False,  mesh=mesh_parameters)




G_loc = G_latt.copy()
G_loc.zero()

for ik in mpi.slice_array(ikarray):
    
    G_latt = SK.lattice_gf(
                    ik=ik, mu=mu, iw_or_w='w', with_Sigma=True, 
                    with_dc=True, broadening=0.0, mesh=mesh_parameters)

        
    G_latt *= SK.bz_weights[ik]

    
    tmp = G_latt.copy()
    tmp.zero()
    
    for bname, gf in G_latt:
        tmp[bname] << G_latt[bname]
    G_loc += tmp


G_loc << mpi.all_reduce(mpi.world, G_loc, lambda x, y: x + y)


if mpi.is_master_node():
    ar['DMFT_results']['Iterations']['G_w_it'+str(iteration_offset-1)] = G_loc
    ar['DMFT_results']['Iterations']['Sigma_w_it'+str(iteration_offset-1)] = sigma[0]
    print 'done writing G and Sigma'

G0_loc = G_latt.copy()
G0_loc.zero()

mu = 5.1748

for ik in mpi.slice_array(ikarray):
    
    G0_latt = SK.lattice_gf(
                    ik=ik, mu=mu, iw_or_w='w', with_Sigma=False, 
                    with_dc=False, mesh=mesh_parameters)
        
    G0_latt *= SK.bz_weights[ik]
    
        
    tmp = G_latt.copy()
    tmp.zero()
    
    for bname, gf in tmp:
        tmp[bname] << G0_latt[bname]
    G0_loc += tmp



G0_loc << mpi.all_reduce(mpi.world, G0_loc, lambda x, y: x + y)
mpi.barrier()


if mpi.is_master_node():
    ar['DMFT_results']['Iterations']['G0_w_it'+str(iteration_offset-1)] = G0_loc
    print 'done writing G0'
#G_loc = SK.extract_G_loc(iw_or_w='w',broadening=0.0)
#G0_loc = SK.extract_G_loc(iw_or_w='w',with_Sigma=False,with_dc=False)






if mpi.is_master_node(): del ar
