from itertools import *
import numpy as np
import pytriqs.utility.mpi as mpi
from pytriqs.archive import *
from pytriqs.gf import *
from triqs_dft_tools.sumk_dft import *
from triqs_dft_tools.sumk_dft_tools import *
from pytriqs.operators.util.hamiltonians import *
from pytriqs.operators.util.U_matrix import *
from triqs_cthyb import *
import ED_lib_interface.interface as E


#E.fit_hyb()

filename = 'H_fBZ.proj'

SK = SumkDFT(hdf_file = filename+'.h5', use_dft_blocks = False)


beta = 50.0 # should be 10... for now this is quicker!
 
Sigma = SK.block_structure.create_gf(beta=beta)
spin_dic = {'up':0,'down':1}
SK.put_Sigma([Sigma])
G = SK.extract_G_loc()
SK.analyse_block_structure_from_gf(G, threshold = 1e-3)
for i_sh in range(len(SK.deg_shells)):
    num_block_deg_orbs = len(SK.deg_shells[i_sh])
    mpi.report('found {0:d} blocks of degenerate orbitals in shell {1:d}'.format(num_block_deg_orbs, i_sh))
    for iblock in range(num_block_deg_orbs):
        mpi.report('block {0:d} consists of orbitals:'.format(iblock))
        for keys in SK.deg_shells[i_sh][iblock].keys():
            mpi.report('  '+keys)
repr_orbs = []
    
num_block_deg_orbs = len(SK.deg_shells[0])
for iblock in range(num_block_deg_orbs):
    repr_orbs.append(SK.deg_shells[0][iblock].keys()[0])

# Setup CTQMC Solver
n_orb = SK.corr_shells[0]['dim']
spin_names = ['up','down']
orb_names = [i for i in range(0,n_orb)]
orb_hyb = False

#gf_struct = set_operator_structure(spin_names, orb_names, orb_hyb)
gf_struct = SK.gf_struct_solver[0]
mpi.report('Sumk to Solver: %s'%SK.sumk_to_solver)
mpi.report('GF struct sumk: %s'%SK.gf_struct_sumk)
mpi.report('GF struct solver: %s'%SK.gf_struct_solver)


edcfg = dict()
edcfg['ea'] = 0.0
edcfg['n_orbs'] = n_orb
edcfg["n bath sites"] = 1
edcfg['n_total'] = n_orb * (1 + edcfg["n bath sites"] )
edcfg['fock min'] = 13
edcfg['fock max'] = 18
edcfg['spin pm'] = 2
edcfg['beta'] = beta
edcfg['NCV'] = 100
edcfg['NEV'] = 7
edcfg['NLANC'] = 100
edcfg['omega min'] = -15.0
edcfg['omega max'] = 15.0
edcfg['n omega'] = 500
edcfg['n matsub'] = len(list(Sigma['up'].mesh))//2
edcfg['boltzmann_cutoff']=1e-15


edcfg['startV'] = [0, 10]
edcfg['startEps'] = [-10, 10]
edcfg['weightFunc'] = 0
edcfg['fit_mats_max'] = 10
edcfg['maxDeltaFitBreak'] = 1e100
edcfg['numItersRandFit'] = 100
edcfg['all_sectors'] = True
edcfg['sectors'] = [[7,8],[8,7],
                    [8,8],
                    [7,9],[9,7],
                    [9,6],[6,9],
                    [7,7]]
edcfg["eigen executable"] = 'anderson'
edcfg["eigen input"] = 'anderson.param'
edcfg["eigen ncores"] = mpi.size


mpi.report('setting solver')
S = Solver(beta=beta, gf_struct=gf_struct)

# Construct the Hamiltonian and save it in Hamiltonian_store.txt
H = Operator() 
U = 8.0
J = 1.0


out = U_J_to_radial_integrals(2, U, J)

mpi.report('setting Coulomb')
U_sph = U_matrix(l=2, U_int=U, J_hund=J)
mpi.report('trafo Coulomb')
U_cubic = transform_U_matrix(U_sph, spherical_to_cubic(l=2, convention=''))
mpi.report('reducing Coulomb')
Umat, Upmat = reduce_4index_to_2index(U_cubic)
mixing = 1.0 # 1.0: do not mix

#H = h_int_density(spin_names, orb_names, map_operator_structure=SK.sumk_to_solver[0], U=Umat, Uprime=Upmat)
#H = h_int_slater(spin_names, orb_names, U_cubic, map_operator_structure=SK.sumk_to_solver[0])


# Print some information on the master node
mpi.report('Greens function structure is: %s '%gf_struct)

# Double Counting: 0 FLL, 1 Held, 2 AMF
DC_type = 0
DC_value = 59.3
#DC_value = None
#DC_value = 58.0

n_iterations = 15

mu_prec = 0.01

# Prepare hdf file and and check for previous iterations
iteration_offset = 0
previous_present = False
if mpi.is_master_node():
    ar = HDFArchive(filename+'.h5','a')
    if not 'DMFT_results' in ar: ar.create_group('DMFT_results')
    if not 'Iterations' in ar['DMFT_results']: ar['DMFT_results'].create_group('Iterations')
    if 'iteration_count' in ar['DMFT_results']: 
        previous_present = True
        iteration_offset = ar['DMFT_results']['iteration_count']+1
        S.Sigma_iw = ar['DMFT_results']['Iterations']['Sigma_it'+str(iteration_offset-1)]
        SK.dc_imp = ar['DMFT_results']['Iterations']['dc_imp'+str(iteration_offset-1)]
        SK.dc_energ = ar['DMFT_results']['Iterations']['dc_energ'+str(iteration_offset-1)]
        SK.chemical_potential = ar['DMFT_results']['Iterations']['chemical_potential'+str(iteration_offset-1)].real
        epsk = ar['DMFT_results']['Iterations']['epsk_it'+str(iteration_offset-1)][...]
        Vk = ar['DMFT_results']['Iterations']['Vk_it'+str(iteration_offset-1)][...]

previous_present = mpi.bcast(previous_present)
iteration_offset = mpi.bcast(iteration_offset)
S.Sigma_iw = mpi.bcast(S.Sigma_iw)
SK.dc_imp = mpi.bcast(SK.dc_imp)
SK.dc_energ = mpi.bcast(SK.dc_energ)
SK.chemical_potential = mpi.bcast(SK.chemical_potential)

SK.chemical_potential = 5.115476

# Calc the first G0
#SK.symm_deg_gf(S.Sigma_iw,orb=0)

SK.put_Sigma(Sigma_imp = [S.Sigma_iw])
SK.calc_mu(precision=mu_prec)
mpi.report('extracting G_loc')
if True:
    S.G_iw << SK.extract_G_loc()[0]
    if mpi.is_master_node():
        ar = HDFArchive(filename+'.h5','a')
        ar['g_iw'] = S.G_iw
else:
    if mpi.is_master_node():
        ar = HDFArchive(filename+'.h5','r')
        S.G_iw = ar['g_iw']

#if mpi.is_master_node():
#    ar = HDFArchive(filename+'.h5','a')
#    ar['g_iw'] = S.G_iw
SK.symm_deg_gf(S.G_iw, orb=0)

#Init the DC term and the self-energy if no previous iteration was found
if iteration_offset == 0:
    dm = S.G_iw.density()
    SK.calc_dc(dm, U_interact=U, J_hund=J, orb=0, use_dc_formula=DC_type,use_dc_value=DC_value)
    S.Sigma_iw << SK.dc_imp[0]['up'][0,0]

mpi.report('%s DMFT cycles requested. Starting with iteration %s.'%(n_iterations,iteration_offset))

# The infamous DMFT self consistency cycle
for it in range(iteration_offset, iteration_offset + n_iterations):
    
    mpi.report('\nDoing iteration: %s'%it)
    
    # Get G0
    mpi.report('getting G0')
    S.G0_iw << inverse(S.Sigma_iw + inverse(S.G_iw))
    
    # Solve the impurity problem
    #eal = SK.eff_atomic_levels()[0]
    # rather get eal from the tail of the hybridization
    eal = {'up':  np.zeros((n_orb,n_orb)),
           'down':np.zeros((n_orb,n_orb))}

    Delta_iw = 0*S.G0_iw    
    Delta_iw << iOmega_n
    Delta_iw -= inverse(S.G0_iw)
    
    
    for key in SK.gf_struct_solver[0]:
        spin, orb = key.split('_')
        orb = int(orb)
        a = Delta_iw[key].fit_tail()
        Delta_iw[key] -= a[0][0][0,0]
        eal[spin][orb,orb] = a[0][0][0,0].real
    G0_iw_F = 0*S.G0_iw
    if mpi.is_master_node():

        # if no previous calculation is present, do multiple random starts for 
        # optimizing the fit
        # else, use previous solution as starting guess.
        if (iteration_offset == 0) and ( it == 0 ):
            guess = None
        else:
            guess = [epsk,Vk]
        fit_param = E.fit_hyb(Delta_iw,edcfg,repr_orbs,guess)
        #fit_param = np.array([[3.37, -2.0],[-3.92,1.17]])

        epsk, Vk = E.distr_fit_to_deg_orbs(fit_param,
                                           SK.gf_struct_solver[0], 
                                           SK.deg_shells[0],n_orb)
        ar['DMFT_results']['Iterations']['epsk_it'+str(it)] = epsk
        ar['DMFT_results']['Iterations']['Vk_it'+str(it)] = Vk

        mpi.report('setting sol')
        E.set_solver(U_cubic,eal,Vk,epsk,edcfg)
        Delta_iw_F = 0*S.G0_iw
        G0_iw_F = 0*S.G0_iw
        
        for key in SK.gf_struct_solver[0]:
            spin, orb = key.split('_')
            orb = int(orb)

            Delta_iw_F[key] += E.Delta_fit_eps_V(epsk[0,orb,spin_dic[spin]],
                                                 Vk[0,orb,spin_dic[spin]],
                                                 Delta_iw_F[key])
            G0_iw_F[key] << iOmega_n
            G0_iw_F[key] -= Delta_iw_F[key]
            G0_iw_F[key] -= eal[spin][orb,orb]
            G0_iw_F[key] = inverse(G0_iw_F[key])
        ar['DMFT_results']['Iterations']['eal_it'+str(it)] = eal
        ar['DMFT_results']['Iterations']['Delta_F_it'+str(it)] = Delta_iw_F
        ar['DMFT_results']['Iterations']['Delta_it'+str(it)] = Delta_iw
        ar['DMFT_results']['Iterations']['G0_iw_loc_F_it'+str(it)] = G0_iw_F
        ar['DMFT_results']['Iterations']['G0_iw_loc_it'+str(it)] = S.G0_iw
    import time
    start = time.time()
    if mpi.is_master_node():
        E.remove_old_run()
    mpi.barrier()
    if mpi.is_master_node():
        E.run_solver(edcfg)
    E.wait_solver(mpi.rank)
    mpi.report('solver took',time.time() -start)
    E_occ = []
    if mpi.is_master_node():
        E_occ = E.read_static('N')
        E_occ_up = E.read_static('N_up')
        E_occ_dn = E.read_static('N_dn')
        #mpi.report('orbital atomic occupation from ED (up)',list(E_occ_up))
        #mpi.report('orbital atomic occupation from ED (dn)',list(E_occ_dn))
        #mpi.report('total atomic occupation from ED',np.sum(E_occ))
    E_occ = mpi.bcast(E_occ)
    mpi.barrier()
    if mpi.is_master_node():
        G = E.read_matsub_data(edcfg)
        # put G data into triqs GF object
        
        for iOrb,key in enumerate(SK.gf_struct_solver[0]):
            spin, orb = key.split('_')
            orb = int(orb)
            S.G_iw[key].data[:,0,0] = G[:,orb,spin_dic[spin]]
            S.G_iw[key].fit_tail()
        ar['DMFT_results']['Iterations']['Sigma_F_it'+str(it)] = 0.0
    

    
    S.G_iw = mpi.bcast(S.G_iw)
    G0_iw_F = mpi.bcast(G0_iw_F)
    dm_new = S.G_iw.density()
    mpi.report('Total impurity charge <N>: {0:1.5f}, matsub summation: {1:1.5f} '.format(np.sum(E_occ).real, S.G_iw.total_density().real))
    for sig,gf in S.G_iw:
        mpi.report("Orbital new %s density: %.6f"%(sig,dm_new[sig][0,0].real))

    if abs(np.sum(E_occ).real - S.G_iw.total_density().real) > 0.1:
        mpi.report('large difference between observables and matsubsara summation! aborting! <N>: {0:1.5f}, matsub: {1:1.5f} '.format(np.sum(E_occ).real, S.G_iw.total_density().real))
        raise Exception('matsub error')
    # the calculation of the self-energy should be done with the non-interacting greens function
    # of the impurity, i.e., the discretized one!
    S.Sigma_iw = inverse(G0_iw_F) - inverse(S.G_iw)
    
    # Now mix Sigma and G with factor Mix, if wanted:
    if (it>1 or previous_present):
        if (mpi.is_master_node() and (mixing<1.0)):
            mpi.report("Mixing Sigma and G with factor %s"%mixing)
            S.Sigma_iw << mixing * S.Sigma_iw + (1.0 - mixing) * ar['DMFT_results']['Iterations']['Sigma_it'+str(it-1)]
            S.G_iw << mixing * S.G_iw + (1.0 - mixing) * ar['DMFT_results']['Iterations']['Gimp_it'+str(it-1)]
        S.G_iw << mpi.bcast(S.G_iw)
        S.Sigma_iw << mpi.bcast(S.Sigma_iw)


    mpi.barrier()

    if mpi.is_master_node(): 
        ar['DMFT_results']['Iterations']['Gimp_it'+str(it)] = S.G_iw
        ar['DMFT_results']['Iterations']['Sigma_uns_it'+str(it)] = S.Sigma_iw
    # Calculate double counting
    dm = S.G_iw.density()
    SK.calc_dc(dm, U_interact=U, J_hund=J, orb=0, use_dc_formula=DC_type,use_dc_value=DC_value)
    # Get new G
    SK.symm_deg_gf(S.Sigma_iw,orb=0)
    SK.put_Sigma(Sigma_imp=[S.Sigma_iw])
    SK.calc_mu(precision=mu_prec)

    S.G_iw << SK.extract_G_loc()[0]
    
    
    # print densities
    #dm_new = S.G_iw.density()
    for sig,gf in S.G_iw:
        mpi.report("Orbital %s density: %.6f"%(sig,dm[sig][0,0].real))
        #mpi.report("Orbital new %s density: %.6f"%(sig,dm_new[sig][0,0].real))

    mpi.report('Total charge of Gloc : %.6f'%S.G_iw.total_density().real)

    if mpi.is_master_node(): 
        ar['DMFT_results']['iteration_count'] = it
        ar['DMFT_results']['Iterations']['Sigma_it'+str(it)] = S.Sigma_iw
        ar['DMFT_results']['Iterations']['Gloc_it'+str(it)] = S.G_iw
        ar['DMFT_results']['Iterations']['G0loc_it'+str(it)] = S.G0_iw
        ar['DMFT_results']['Iterations']['dc_imp'+str(it)] = SK.dc_imp
        ar['DMFT_results']['Iterations']['dc_energ'+str(it)] = SK.dc_energ
        ar['DMFT_results']['Iterations']['chemical_potential'+str(it)] = SK.chemical_potential

if mpi.is_master_node(): del ar
