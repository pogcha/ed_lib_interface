#!/bin/bash
   #PBS -N NiO_csc
   #PBS -l nodes=1:ppn=20
   #PBS -q express
   #PBS -j oe
   #PBS -M s_gogkji@uni-bremen.de
   #PBS -m ae
   export OMP_NUM_THREADS=1
   
   source /home1/mschueler/start_triqs20.sh

   cd $PBS_O_WORKDIR
   echo ##this machinefile was used:
   cat $PBS_NODEFILE
   echo "startet at "`date`
   NUM_PROCS=`cat $PBS_NODEFILE|wc -l`
   echo $NUM_PROCS
   #mpirun -np $NUM_PROCS python
   triqs_vasp_csc.sh -n $NUM_PROCS -i 20 -v vasp_intel.sh -d NiO.py

   echo "ended at "`date`
