from triqs_dft_tools.converters.hk_csc_converter import *
Converter = Hk_csc_Converter(filename = 'H_fBZ.proj',weights_filename='N.proj', proj_filename='P_fBZ.proj')
Converter.convert_dft_input(only_upper_triangle=False)
